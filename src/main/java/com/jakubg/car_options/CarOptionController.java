/**
 @author: Jakub Gzyl, 2021
 **/
package com.jakubg.car_options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CarOptionController {

    @Autowired
    private CarOptionService carOptionService;

    @GetMapping("/car/options/{carId}")
    public List<CarOption> getOptions(@PathVariable long carId) {

        return this.carOptionService.getOptions(carId);
    }

    @PostMapping("/car/options/{carId}")
    public List<CarOption> addOptions(@RequestBody List<CarOption> carOptions, @PathVariable long carId) {
        return this.carOptionService.addOptions(carOptions, carId);
    }

}
