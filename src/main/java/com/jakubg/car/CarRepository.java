/**
 @author: Jakub Gzyl, 2021
 **/
package com.jakubg.car;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CarRepository extends CrudRepository<Car, Long> {

    List<Car> findAll();
}
