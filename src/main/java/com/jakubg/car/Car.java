/**
@author: Jakub Gzyl, 2021
 **/
package com.jakubg.car;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;

@Data
public class Car {

    @Id
    private long id;

    private String make;
    private String model;
    private String edition;
    private BigDecimal price;
}

