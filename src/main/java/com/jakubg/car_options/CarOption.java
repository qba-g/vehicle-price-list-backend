/**
 @author: Jakub Gzyl, 2021
 **/
package com.jakubg.car_options;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;

@Data
public class CarOption {

    @Id
    private long id;

    private long carId;

    private String name;
    private BigDecimal price;
}
