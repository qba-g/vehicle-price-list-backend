/**
 @author: Jakub Gzyl, 2021
 **/
package com.jakubg.car_options;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CarOptionRepository extends CrudRepository<CarOption, Long> {

    List<CarOption> findByCarId(long carId);

}
