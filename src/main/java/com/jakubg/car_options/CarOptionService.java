/**
 @author: Jakub Gzyl, 2021
 **/
package com.jakubg.car_options;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class CarOptionService {

    @Autowired
    private CarOptionRepository carOptionRepository;

    public List<CarOption> getOptions(long id) {
        return this.carOptionRepository.findByCarId(id);
    }

    @Transactional
    public List<CarOption> addOptions(List<CarOption> newCarOptions, long carId) {

        if (newCarOptions != null && newCarOptions.size() > 0) {

            List<CarOption> optionsToDelete = this.carOptionRepository.findByCarId(carId);
            this.carOptionRepository.deleteAll(optionsToDelete);

            newCarOptions.stream().forEach(item -> item.setId(0));
            this.carOptionRepository.saveAll(newCarOptions);
            List<CarOption> newDbCarOptions = this.carOptionRepository.findByCarId(carId);
            return newDbCarOptions;
        }else {
            List<CarOption> optionsToDelete = this.carOptionRepository.findByCarId(carId);
            this.carOptionRepository.deleteAll(optionsToDelete);
        }

        return Collections.emptyList();
    }

}
