package com.jakubg.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
@author: Jakub Gzyl, 2021
**/

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping("/car/list")
    public List<Car> getCarList() {
        return this.carService.getCarList();
    }

    @PostMapping("/car")
    public Car addCar(@RequestBody Car newCar) {
        return this.carService.addCar(newCar);
    }

    @DeleteMapping("/car/{carId}")
    public void deleteCar(@PathVariable long carId) {
        this.carService.deleteCar(carId);
    }

    @PutMapping("/car")
    public Car updateCar(@RequestBody Car updatedCar) {

        return this.carService.updateCar(updatedCar);
    }

}
