/**
 @author: Jakub Gzyl, 2021
 **/
package com.jakubg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehiclePriceListApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehiclePriceListApplication.class, args);
	}

}
