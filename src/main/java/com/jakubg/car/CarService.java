/**
 @author: Jakub Gzyl, 2021
 **/

package com.jakubg.car;

import com.jakubg.car_options.CarOption;
import com.jakubg.car_options.CarOptionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
@Transactional
@Slf4j
public class CarService {

    @Autowired
    private CarRepository carRepository;
    @Autowired
    private CarOptionRepository carOptionsRepository;

    public List<Car> getCarList() {
        return this.carRepository.findAll();
    }

    public Car addCar(Car newCar) {
        return this.carRepository.save(newCar);
    }

    @Transactional
    public void deleteCar(long carId) {
        List<CarOption> optionsToDelete = this.carOptionsRepository.findByCarId(carId);
        this.carOptionsRepository.deleteAll(optionsToDelete);
        this.carRepository.deleteById(carId);
    }

    public Car updateCar(@RequestBody Car updatedCar) {
        return this.carRepository.save(updatedCar);
    }
}
